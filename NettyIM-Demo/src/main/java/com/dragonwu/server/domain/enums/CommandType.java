package com.dragonwu.server.domain.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author DragonWu
 * @since 2023-01-05 14:38
 * 聊天的命令枚举
 **/
@Getter
@AllArgsConstructor
public enum CommandType {
    CONNECTION(1001),
    CHAT(1002),
    JOIN_GROUP(1003),
    ERROR(-1);

    private final Integer code;

    public static CommandType match(Integer code) {
        for (CommandType value : CommandType.values()) {
            if (value.getCode().equals(code)) {
                return value;
            }
        }
        return ERROR;
    }
}
