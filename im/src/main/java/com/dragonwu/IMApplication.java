package com.dragonwu;

import com.dragonwu.common.basic.handler.WarnHandler;
import com.dragonwu.im.server.IMServerStarter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * @author Dragon Wu
 * @since 2023/1/23 12:16
 */
@SpringBootApplication
public class IMApplication {

    public static void main(String[] args) throws Exception {
        WarnHandler.disableAccessWarnings(); //忽略非法反射警告
        ConfigurableApplicationContext context = SpringApplication.run(IMApplication.class, args);
        IMServerStarter starter = context.getBean(IMServerStarter.class);
        starter.start(); //启动netty服务器
    }
}
