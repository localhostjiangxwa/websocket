package com.dragonwu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Dragon Wu
 * @since 2022-10-12 19:06
 **/
@SpringBootApplication
public class WebsocketFrameApplication {
    public static void main(String[] args) {
        SpringApplication.run(WebsocketFrameApplication.class,args);
    }
}
